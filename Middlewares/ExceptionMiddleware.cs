﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MyToList.Models.Exception;
using MyToList.Services;
using Newtonsoft.Json;
using NLog;
using Serilog;
using ILogger = NLog.ILogger;

namespace MyToList.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value!.Contains("/api/"))
            {
                try
                {
                    await _next(context);
                }
                catch (NotFoundException ex)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.NotFound;
                    var response = JsonConvert.SerializeObject(new ExceptionResponse
                    {
                        Message = ex.Message,
                        Trace = ex.StackTrace,
                        Type = ex.GetType().Name
                    });

                    await context.Response.WriteAsync(response);
                    _logger.Info(response);
                    Log.Error(ex, "Not found exception");
                }
                catch (CustomException ex)
                {
                    context.Response.StatusCode = (int) ex.Status;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(ex.Value));
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new ExceptionResponse
                    {
                        Message = ex.Message,
                        Trace = ex.StackTrace,
                        Type = ex.GetType().Name
                    }));
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}
