﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MyToList.Models.Database;
using MyToList.Models.Database.Entities;

namespace MyToList.Repositories
{
    public class ListEntityRepository : BaseRepository<ListEntity>
    {
        public ListEntityRepository(MyToDoListDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<ListEntity>> Search(string text)
        {
            return await Table
                .Where(p => p.Name.Contains(text))
                .ToListAsync();
        }
    }
}