﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyToList.Models;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace MyToList.Services
{
    public class EmailService
    {
        private readonly EmailConfiguration _emailConfiguration;
        private readonly SendGridClient _client;

        public EmailService(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
            _client = new SendGridClient(emailConfiguration.ApiKey);
        }

        public async Task<bool> SendTestEmail(string message)
        {
            var msg = MailHelper.CreateSingleEmail(
                new EmailAddress(_emailConfiguration.FromAddress, "CST"),
                new EmailAddress("cristian.lina@cst.ro", "Client"),
                "Test Subject",
                message,
                null);

            var response = await _client.SendEmailAsync(msg);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }

        public async Task<bool> SendEmailConfirmation(string email, string code)
        {
            var message = MailHelper.CreateSingleTemplateEmail(
                new EmailAddress(_emailConfiguration.FromAddress, "CST"),
                new EmailAddress(email, "Client"),
                _emailConfiguration.EmailConfigurationTemplateId,
                new Dictionary<string, string>
                {
                    {"confirm-code", code},
                    {"current-year", DateTime.Now.Year.ToString()}
                });

            var response = await _client.SendEmailAsync(message);

            if (response.IsSuccessStatusCode)
                return true;

            return false;
        }
    }
}
