﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using MyToList.Models.Database.Entities;
using MyToList.Models.Requests;
using MyToList.Models.Responses.List;
using MyToList.Repositories;
using AutoMapper;
using MyToList.Models.Constants;
using MyToList.Models.Exception;

namespace MyToList.Services
{
    public class ListEntityService : BaseService<ListEntity>
    {
        private readonly ListEntityRepository _listEntityRepository;
        private readonly BasePaginationRequest _pagination;
        private readonly IMapper _mapper;
        private readonly IServiceProvider _serviceProvider;
        private readonly EmailService _emailService;

        public ListEntityService(ListEntityRepository listEntityRepository,
            IHttpContextAccessor contextAccessor,
            IMapper mapper,
            IServiceProvider serviceProvider,
            EmailService emailService) 
            : base(listEntityRepository, contextAccessor)
        {
            _listEntityRepository = listEntityRepository;
            _mapper = mapper;
            _serviceProvider = serviceProvider;
            _emailService = emailService;
            _pagination = (BasePaginationRequest) contextAccessor.HttpContext.Items["pagination"];
        }

        public async Task<List<ListDetailsResponse>> GetLists(int userId)
        {
            //var service = (ListEntityRepository) _serviceProvider.GetService(typeof(ListEntityRepository));
            var result = await Get(p => p.UserId == userId)
                .Skip(_pagination.Size * (_pagination.Page - 1))
                .Take(_pagination.Size)
                //.IgnoreQueryFilters()
                .ToListAsync();

            return _mapper.Map<List<ListDetailsResponse>>(result);
        }

        public async Task<ListDetailsResponse> GetList(int userId, int listId)
        {
            await _emailService.SendTestEmail("Test email");

            var result = await Get(p => p.UserId == userId && p.Id == listId)
                .FirstOrDefaultAsync();

            if (result == null)
                //throw new CustomException(_mapper.Map<ListDetailsResponse>(await Get().FirstOrDefaultAsync()), HttpStatusCode.NotFound);
                throw new NotFoundException(StringConstants.ListNotFound);

            return _mapper.Map<ListDetailsResponse>(result);
        }

        public async Task<List<ListEntity>> Search(string text)
        {
            return await _listEntityRepository.Search(text);
        }
    }
}
