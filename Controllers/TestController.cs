﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyToList.Helpers;
using MyToList.Services;

namespace MyToList.Controllers
{
    [ApiController]
    [Route("/api/Test")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public async Task<ObjectResult> Get()
        {
            var factory = new TaskFactory();

            var string1 = new StringBuilder();
            var string2 = new StringBuilder();
            var taskList = new List<Task>();
            var list = new List<int>();

            for (int i = 0; i < 100; i++)
            {
                string1.Append($"{i} ");

                taskList.Add(Task.Run(() =>
                {
                    Thread.Sleep(1000);
                    string2.Append($"{i} ");
                    list.Add(i);
                }));
            }

            //Debug.WriteLine(string1.ToString());
            Task.WaitAll(taskList.ToArray());
            Debug.WriteLine(string2.ToString());
            Debug.WriteLine(list.Count);

            Debug.WriteLine(GetFromList(list));

            return Ok(string2.ToString());
        }

        private int GetFromList(List<int> list)
        {
            return list.FirstOrDefault();
        }

    }
}
