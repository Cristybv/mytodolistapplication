﻿using System.Collections.Generic;
 using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyToList.Helpers;
using MyToList.Models.Exception;
using MyToList.Models.Requests;
using MyToList.Services;
using NLog;
using Serilog;

namespace MyToList.Controllers
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpGet]
        public async Task<ObjectResult> GetUserDetails()
        {
            return Ok(await _userService.GetUserDetails(User.GetUserId()));
        }

        [HttpPost("Register")]
        public async Task<ObjectResult> Register([FromBody] UserRegisterRequest userRequest,
            [FromQuery] string role)
        {
            return Ok(await _userService.RegisterUser(userRequest, role));
        }

        [HttpPost("Login")]
        public async Task<ObjectResult> Login([FromBody] UserLoginRequest userRequest)
        {
            throw new NotFoundException("Text not found");
            return Ok(await _userService.Login(userRequest.Username, userRequest.Password));
        }

        [HttpPut("Token/Refresh")]
        public async Task<ObjectResult> RefreshToken([FromBody] RefreshTokenRequest refreshTokenRequest)
        {
            return Ok(await _userService.RefreshToken(refreshTokenRequest.RefreshToken));
        }

        [HttpPut("Token/Revoke")]
        public async Task<ObjectResult> RevokeToken([FromBody] RefreshTokenRequest refreshTokenRequest)
        {
            return Ok(await _userService.RevokeRefreshToken(refreshTokenRequest.RefreshToken));
        }

        [HttpPost("Register/ResendEmail/{email}")]
        public async Task<ObjectResult> ResendEmail([FromRoute] string email)
        {
            return Ok(await _userService.ResendEmailConfirmation(email));
        }

        [HttpPut("Register/ConfirmEmail")]
        public async Task<ObjectResult> ConfirmEmail([FromQuery] string email, [FromQuery] string code)
        {
            return Ok(await _userService.ConfirmEmail(email, code));
        }
    }
}