﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyToList.Filters;
using MyToList.Helpers;
using MyToList.Models.Database.Entities;
using MyToList.Models.Enum;
using MyToList.Models.Requests.List;
using MyToList.Models.Responses.List;
using MyToList.Services;

namespace MyToList.Controllers
{
    [Authorize(Roles = "Normal")]
    [ApiController]
    [Route("api/list")]
    public class ListEntityController : ControllerBase
    {
        private readonly ListEntityService _listEntityService;
        private readonly UserService _userService;
        private readonly IMapper _mapper;

        public ListEntityController(ListEntityService listEntityService,
            UserService userService,
            IMapper mapper)
        {
            _listEntityService = listEntityService;
            _userService = userService;
            _mapper = mapper;
        }

        [PaginationResourceFilter]
        [HttpGet]
        public async Task<ObjectResult> GetAll()
        {
            return Ok(await _listEntityService.GetLists(User.GetUserId()));
        }

        [HttpGet("{listId}")]
        public async Task<ObjectResult> GetById([FromRoute] int listId)
        {
            return Ok(await _listEntityService.GetList(User.GetUserId(), listId));
        }

        [HttpPut("{listId}")]
        public async Task<ObjectResult> UpdateList([FromRoute] int listId,
            [FromBody] UpdateListRequest list)
        {
            var normalUser = await _userService.Get(p => p.Id == User.GetUserId())
                .Include(p => p.Lists.Where(q => q.Id == listId))
                .FirstOrDefaultAsync();

            if (normalUser?.Lists.FirstOrDefault() == null)
                return Ok(null);

            return Ok(await _listEntityService.Update(_mapper.Map(list, normalUser.Lists.First())));
        }

        [HttpDelete("{listId}")]
        public async Task<ObjectResult> DeleteList([FromRoute] int listId)
        {
            var normalUser = await _userService.Get(p => p.Id == User.GetUserId())
                .Include(p => p.Lists.Where(q => q.Id == listId))
                .FirstOrDefaultAsync();

            if (normalUser?.Lists.FirstOrDefault() == null)
                return Ok(null);

            return Ok(await _listEntityService.Delete(normalUser.Lists.First()));
        }

        [HttpPost]
        public async Task<ObjectResult> CreateList([FromBody] ListEntity list)
        {
            return Ok(await _listEntityService.Create(list));
        }
    }
}