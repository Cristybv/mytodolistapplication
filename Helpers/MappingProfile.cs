﻿using AutoMapper;
using MyToList.Models.Database.Entities;
using MyToList.Models.Requests.List;
using MyToList.Models.Responses.List;

namespace MyToList.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UpdateListRequest, ListEntity>()
                .ForAllMembers(p => p.Condition((q, s, m) => m != null));
            CreateMap<ListEntity, ListDetailsResponse>();
        }
    }
}
