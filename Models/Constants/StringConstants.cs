﻿namespace MyToList.Models.Constants
{
    public static class StringConstants
    {
        public static string NotFound = "Not Found";
        public static string ListNotFound = "List not found";
    }
}
