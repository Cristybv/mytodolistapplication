﻿using Newtonsoft.Json;

namespace MyToList.Models.Exception
{
    public class ExceptionResponse
    {
        public string Message { get; set; }
        public string Type { get; set; }
        [JsonIgnore] public string Trace { get; set; }
    }
}
