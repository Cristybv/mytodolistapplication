﻿namespace MyToList.Models.Exception
{
    public class BadRequestException : System.Exception
    {
        public BadRequestException(string message = "Bad request") : base(message)
        {
        }
    }
}
