﻿namespace MyToList.Models.Exception
{
    public class NotFoundException : System.Exception
    {
        public NotFoundException(string message = "NotFound") : base(message)
        {
        }
    }
}
