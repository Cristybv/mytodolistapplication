﻿using System.Net;

namespace MyToList.Models.Exception
{
    public class CustomException : System.Exception
    {
        public CustomException(object? value, HttpStatusCode statusCode)
        {
            Value = value;
            Status = statusCode;
        }

        public object? Value { get; set; }
        public HttpStatusCode Status { get; set; }

    }
}
