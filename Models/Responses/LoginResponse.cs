﻿using Microsoft.AspNetCore.Identity;
using MyToList.Models.Database.Entities;

namespace MyToList.Models.Responses
{
    public class LoginResponse
    {
        public SignInResult Result { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public UserEntity User { get; set; }
    }
}
