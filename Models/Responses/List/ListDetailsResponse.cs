﻿using MyToList.Models.Database.Entities;

namespace MyToList.Models.Responses.List
{
    public class ListDetailsResponse : BaseResponse
    {
        public string Name { get; set; }

        /*public static ListDetailsResponse ListToDto(ListEntity entity)
        {
            return new ListDetailsResponse
            {
                Id = entity.Id,
                DateCreated = entity.DateCreated,
                Name = entity.Name
            };
        }*/
    }
}