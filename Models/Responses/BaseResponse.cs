﻿using System;

namespace MyToList.Models.Responses
{
    public class BaseResponse
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
