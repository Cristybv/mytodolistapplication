﻿namespace MyToList.Models.Requests.List
{
    public class UpdateListRequest
    {
        public string Name { get; set; }
        public bool? IsCompleted { get; set; }
    }
}
