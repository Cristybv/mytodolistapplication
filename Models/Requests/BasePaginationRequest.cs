﻿namespace MyToList.Models.Requests
{
    public class BasePaginationRequest
    {
        public int Size { get; set; }
        public int Page { get; set; } = 1;
        public string OrderBy { get; set; }
        public bool Desc { get; set; }
    }
}
