﻿namespace MyToList.Models.Requests
{
    public class RefreshTokenRequest
    {
        public string RefreshToken { get; set; }
    }
}