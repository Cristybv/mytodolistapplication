﻿namespace MyToList.Models
{
    public class EmailConfiguration
    {
        public string ApiKey { get; set; }
        public string FromAddress { get; set; }
        public string EmailConfigurationTemplateId { get; set; }
    }
}
