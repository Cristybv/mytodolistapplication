﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace MyToList.Models.Database.Entities
{
    [Table("lists")]
    public class ListEntity : BaseEntity
    {
        public string Name { get; set; } = "NameDefault";
        public bool IsCompleted { get; set; } = false;
        public int UserId { get; set; }
        [ForeignKey("UserId")] public UserEntity User { get; set; }
        public List<TaskEntity> TaskLists { get; set; }
    }
}
