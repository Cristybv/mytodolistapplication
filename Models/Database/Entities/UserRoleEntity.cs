﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace MyToList.Models.Database.Entities
{
    public class UserRoleEntity : IdentityUserRole<int>
    {
        [ForeignKey("UserId")] public UserEntity User { get; set; }
        [ForeignKey("RoleId")] public RoleEntity Role { get; set; }
    }
}
