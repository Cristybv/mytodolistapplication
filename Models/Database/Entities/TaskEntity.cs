﻿using System.ComponentModel.DataAnnotations.Schema;


namespace MyToList.Models.Database.Entities
{
    [Table("tasks")]
    public class TaskEntity : BaseEntity
    {
        public string Name { get; set; }
        public int ListId { get; set; }
        [ForeignKey("ListId")] public ListEntity List { get; set; }
    }
}
